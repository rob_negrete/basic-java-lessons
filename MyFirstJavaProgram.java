public class MyFirstJavaProgram {

    public static void main(String[] args){
        //Create a HelloWorld Object
        //nothing will be executed on this line
        HelloWorld object = new HelloWorld();

        //Call sayHello from HelloWorld
        object.sayHello();

        object.uselessMethod();
    }


}