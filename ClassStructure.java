
// Class Declaration
public class ClassStructure { //Class's Opening bracket

    //This is a line comment. Line coments are just some     
    //quick hints about what your code is doing.
    //or disable one line of code.


    /*
        This is a block comment. 
        This kind of comments are intended to document your code.
        Or also disable an entire block of code, 1 or more lines.
    */

    /** 
     * Class Variables Section
     * 
     * Could be Zero or more variables    
     */
    
    // This is a variable called 'aString'; 
    //This variable will hold 'String' values. Strings are text.
    String aString = "X";
    
    //This is a variable called 'aInt'; 
    //This variable will hold 'integer' values. Integers are numbers without decimals.
    int aInt = 100;

    //This variable is called 'aFloat'
    //This variable will hold 'float' values. Floats ar numbers with decimales.
    float aFloat;


    /**
     * Constructor methods section
     * Constructors allow you to init some default behavior
     */
    
    //This constructor setups default var3 value.
    public ClassStructure( ){//Constructor opening bracket.
        aFloat = aInt * 5;
    }//Constructor closing bracket.

    /** 
     * Methods section
     * You can define 0 or more methods in a class. 
     * It doesn't matter where you place them nor the order.
     */
     
    //void means this methods does not return a value
    //This method does not receive any parameter as input
    void welcome( ){ //welcome opening brackets
        //method's body
        //Zero or more lines of code could be here.
        System.out.println("Welcome, I'm in welcome");
    }//welcome closing brackets


    //String means this method return a text as an String value.
    //This method does not receive any parameter as input
    String hello(){//hello opening brackets
        //Declaring a String variable called 'someText'
        String someText;
        //Assigning a value to 'someText' variable
        someText = "Hello Pal!";
        //Returning someText variable as result.
        return someText;

    }//hello closing brackets


    /**
     *  This method returns an int value
     *  And also receives a 2 integer values as input.
     */
    int sustract(int a, int b){//sustract opening brackets
        //You can do something with input values...
        int c = a - b;

        //And then return this value as a result.
        return c;

    }//sustract closing brackets

    /**
     * Then we have a main method.
     * Main method is Optional. 
     * Only the main entry class could have a main method.
     * When you run a program with java, this method is the entry point.
     */
    public static void main(String ... args){//main opening bracket
        //Creating a new ClassStructure object called 'cs'
        ClassStructure cs  = new ClassStructure();
        //Calling 'ClassStructure' welcome method.
        cs.welcome();
        //Calling 'ClassStructure' hello method and storing it's result in 'a' variable.
        String a = cs.hello();
        //Displaying 'a' variable value
        System.out.println("a is: " + a);
        //Calling ClassStructure' sustract method and storing it's result in 'x' variable.
        //We are passing 2 parameters as input to this method.
        int x = cs.sustract(5, 2);
        //Displaying 'x' variable value.        
        System.out.println("x is: " + x);

    }//main closing brackets

}//Class Closing bracket.
