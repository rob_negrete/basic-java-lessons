import java.util.Scanner;

public class ReadString {

    public static void main(String ...args){

        String name = null, gender = null, otherThing = null;

        final Scanner sc = new Scanner(System.in);

        System.out.println("What is your gender?");

        gender = sc.next();

        System.out.println("What is your name?");

        if(sc.hasNext()){
            name = sc.next();
        }
               
        System.out.println("Tell me something");
        otherThing = sc.nextLine();        
        if(otherThing.isEmpty()){
            otherThing = sc.nextLine();
        }

        String sex = "girl";
        if(gender.equals("M")){
            sex = "boy";
        }

        System.out.println("Hello " + name + " you are a " + sex + ". You said something interesting: " + 
        otherThing );

    }
}