

/**
 * Imagine you have a sucessful Wholesale Store called J-Mart :P
 * 
 * You already manage a lot of products, and you need to list what products you have an how many of them.
 * 
 * By using Arrays/Lists, conditions and loops you will show that information.
 */

 /**
 * Example:
 * 
 * Having these products in my array/list:
 * 
 * bananas, apples, milk, water, ham, paper, meat, water, milk, ham, water, bananas, milk.
 * 
 * This is my expected output:
 * 
 * bananas: 2
 * apples: 1
 * milk: 3
 * water: 3
 * ham: 2
 * paper: 1
 * 
 */

public class JMart {

    //1.
    //Create an array or list with the name of every product you have in stock. 
    //These are the products you need to store in the array/list:
    //bananas, apples, milk, water, ham, paper, meat.
    //Any of these products can be repeated as many times you want.

    //2.
    //Then you have to loop over the array/list
    //every time you find the same product in your list you have to add increment a counter for that kind of product.


    //3.
    //Finally display how many items you have of each product 



}


