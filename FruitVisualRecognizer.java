
/**
 * Imagine you are programming a fruit visual recognition.
 * 
 * You have several fruits passing by a band and a camera is taking snapshots
 * of them.
 * 
 * When a picture is taken you will determine what kind of fruit is.
 * 
 */

public class FruitVisualRecognizer {

    /*
        This method receives two parameters:
            colorOutside: is the fruit's color outside.
            colorInside: is the fruit's color inside.
        The goal of this method is to select by those colors what 
        kind of fruit is.

        Return the name of the fruit when a match is found. Otherwise, return "unknown".
    */
    String chooseFruit(String colorOutside, String colorInside){        
        //When the fruit is red inside and red outside, it's a tomatoe
        //When the fruit is white inside and red outside, it's an apple
        //When the fruit is red inside and green outside, it's a watermelon 
        //When the fruit is yellow inside and yellow outside, it's a mango
        //When the fruit is yellow inside and orange outside, it's an orange
        //when the fruit is white inside and yellow outside, it's a banana 
        //When there are no matches, It's an unknown fruit.

        //return the matched kind of fruit
        return null;
    }

    //Returns a message telling the kind of fruit.
    String displayFruit(String fruit){
        //When is a known fruit return "The fruit is a ..."
 
        //Otherwise return "I don't know this fruit."
        return null;
    }


    public static void main(String ...args){

        //Create a SelectFruits object.

        // Call the chooseFruit method many times with different combinations.
        //Example:  chooseFruit("green", "green");
        
        // With the result value from chooseFruit method, call displayFruit method and print the result.
        //Example: displayFruit( x );
    }

}